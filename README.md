## I. Getting started

### 1. Install dependencies

Create .env in root, copy content from .env.example

```bash
$ docker-compose run --rm api poetry install
```

### 2. Create database

Migrate database schema to latest:

```bash
$ docker-compose run --rm api alembic upgrade head
```
