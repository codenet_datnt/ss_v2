from pydantic import BaseModel

from fastapi import APIRouter

from models.users import User

router = APIRouter()


class UserDetail(BaseModel):
    id: int
    first_name: str
    last_name: str


class CreateUserPayload(BaseModel):
    first_name: str
    last_name: str


@router.post("/users")
async def create_user(user: CreateUserPayload):
    user_id = await User.create(**user.dict())
    return {"id": user_id}


@router.get("/users/{id}", response_model=UserDetail)
async def get_user(id: int):
    user = await User.get(id)
    return user
