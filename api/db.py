from databases import Database
import sqlalchemy

from config import DATABASE_URL

db = Database(DATABASE_URL)

metadata = sqlalchemy.MetaData()
