"""create_users_table

Revision ID: 14af3ee619b3
Revises: 
Create Date: 2021-05-17 17:37:21.688192

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = "14af3ee619b3"
down_revision = None
branch_labels = None
depends_on = None


def create_users() -> None:
    op.create_table(
        "users",
        sa.Column("id", sa.Integer, primary_key=True),
        sa.Column("first_name", sa.String),
        sa.Column("last_name", sa.String),
    )


def upgrade():
    create_users()


def downgrade():
    op.drop_table("users")
