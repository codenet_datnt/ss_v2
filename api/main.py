from app import app
from routers import api_router


@app.get("/hello")
async def read_root():
    return {"Hello": "World"}


app.include_router(api_router, prefix="/api")
